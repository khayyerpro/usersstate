<?php
  /**
   * Sert à charger tous les controllers et les modèles
   *
   * @param string $appli => Nom de l'application
   * @param string $class => Nom du controller ou du modèle
   * @return none
   * Charge par require_once le bon script php
   */
    function app_autoloader($class) {
        // Var par defaut
            $PathFind = false;
        // On extrait le nom de l'appli séparée par le underscore
            $newClass = substr($class, 0, strrpos($class, '_'));
        // Si c'est Controllers ou Models qui sont appelés
            if ($newClass=="") {
                $newClass = $class;
            }
        // Préparation du path
            for ($i = 1; $i <= 5; $i++) {
                if ($i <= 1) {
                    $Path = "";
                } else {
                    $Path = $Path . "../";
                }
        // Controllers
            if (file_exists($Path . "../controllers/$newClass.php")) {
                require_once($Path . "../controllers/$newClass.php");
                $PathFind = true;
            }

            if (file_exists($Path . "../classes/$newClass.php")) {
                require_once($Path . "../classes/$newClass.php");
                $PathFind = true;
            }
        // Models
            if (file_exists($Path . "../models/$newClass.php")) {
                require_once($Path . "../models/$newClass.php");
                $PathFind = true;
            }
        }
        if ($PathFind = false) {
            $lastError = error_get_last();
            trigger_error("Autoloader message => Class not found : $class - needed in " . $lastError["file"] . "(" . $lastError["line"] . ")", E_USER_ERROR);
        }
    }
// Lancement automatique au lancement
    spl_autoload_register('app_autoloader');
