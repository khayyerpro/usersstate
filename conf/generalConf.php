<?php 

/* ----------------------------------------------------------------------------------------------------------------
    INFO SERVEUR 
---------------------------------------------------------------------------------------------------------------- */                                        
// echo __DIR__ . "<br>"; // Chemin litteral 
// echo $_SERVER['SERVER_NAME']."<br>"; // Affiche le nom du serveur 
// echo $_SERVER['REMOTE_ADDR']."<br>"; // Affiche l'adresse IP du serveur
// die("Die ici ==> ".$_SERVER['PHP_SELF']); /* */

/* ----------------------------------------------------------------------------------------------------------------
    CONFIGURATION LOCALE
---------------------------------------------------------------------------------------------------------------- */
if ($_SERVER['SERVER_NAME']=="coursphpn1" OR $_SERVER['REMOTE_ADDR']=="::1") {
// Texte de test
   // echo "prise en compte de la configuration de l'environnement de dev.";
// Conf Database
    define('HOST_DATABASE', '127.0.0.1');
    define('USERNAME_DATABASE', 'root');
    define('PASSWORD_DATABASE', '');
    define('CHARSET_BDD', 'UTF8');
// Nom Bdd Application
    define('DATABASE_NAME_N1', 'bdd_cours_php_n1'); // Bdd copie de la prod
// Définition du path en local de la racine de l'application
    define('PATH_MACHINE',  'C:/Users/Mohamed/Desktop/coursphpn1/');
// Définition du path du host principal
    define('HTTP_PATH_HOST_PRINCIPAL', 'http://coursphpn1/');
// Variable de sécurité pour authentification
    define('VAR_SECURE_AUTH', 'a1szgr5s5mrzlqlveszeikswqs^ùze*rs');
// Langue par défaut
    define('LANGUAGE_DEFAULT', 'fra');

/* ----------------------------------------------------------------------------------------------------------------
    PROD CONFIGURATION
---------------------------------------------------------------------------------------------------------------- */
} elseif ($_SERVER['SERVER_NAME']=="" OR $_SERVER['SERVER_NAME']=="" OR $_SERVER['REMOTE_ADDR']=="") {

/* ----------------------------------------------------------------------------------------------------------------
    ERROR
---------------------------------------------------------------------------------------------------------------- */
} else {
    die("Une erreur c'est produite. ERROR A00000001.");
}