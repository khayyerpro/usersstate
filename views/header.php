<?php
// PARTIE EN TEST !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// Initialisation var
$auth = "ok";
$detect;
// Creation de la session
    $resultUserSession = Controllers::createUserSession($auth);
// Si la creation de la session a échoué
    if ($resultUserSession!=true) {
        echo "La création de la session a échoué !";
        $resultDestroyUserSession =  TestConnexion::destroyUserSession();
    } else {
        echo "La création de la session a réussi !";   
        if (isset($_GET["connect"])){
           $_SESSION['statutClient'] = 1;
           header('../Location:views/index.php');
          
        }
        if (isset($_GET["deconnect"])){
            header('../Location:views/index.php');
            session_destroy();
         }
        
    }


// PARTIE EN TEST !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?>
<!DOCTYPE html>
<html lang="fr">
<?php
// Inclusion du header seul
    require_once ("headerAlone.php");
?>
<body>

<!-- Entete avec logo et Connexion/Deconnexion -->
<div class="container col-12">
    <div class="container col-12">
        <div class="row justify-content-between">
            <div class="logo">
                <a href=""><img class="logoHomePage" src="<?php echo HTTP_PATH_HOST_PRINCIPAL ?>component/img/logoTest01.jpg" alt="Logo test" title=""></a>
                <a class="familyLogoText orange" href="" title=""> Test </a>
                <a class="familyLogoText gris" href="" title=""> Test 2 </a>
            </div>
        </div>
    </div>

<!-- Barre de navigation pour le menu header -->
    <form action="index.php" method="GET">
        <nav class="navbar navbar-dark bg-dark mt-3 mb-1">
            <a class="navbar-brand navTextMenu" data-toggle="collapse" data-target="#navbarSupported" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" href="#"></a>
            <?php 
                echo $_SESSION["statutClient"];
                $detect = new DetectConnect($_SESSION["statutClient"], $_SESSION["prenom"]);    
                $detect->connectStateUser();
            ?>
     </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupported" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupported">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active"><a class="navbar-brand nav-link active" href="">Texte blabla</a></li>
                    <?php
                    // Affichage de la liste des services actifs
                        if ($listeServiceValideParIdClient!=FALSE) {
                            echo "<li class='nav-item sousTitreNavItem'>".$txtLangue->txtServiceActif."</li>";
                            foreach ($listeServiceValideParIdClient as $serviceId => $serviceName) {
                                echo "<li class='nav-item'><a class='listeItemNav' href='".HTTP_PATH_HOST_PRINCIPAL.HTTP_PATH_SERVICES.$serviceId."/".HTTP_PATH_VIEWS."' ><i class='fas fa-chevron-right sm'></i> $serviceName</a></li>";
                            }
                        }
                    ?>
                <li class="nav-item"><a class="navbar-brand" href="" data-toggle="modal" data-target="#exampleModal">Tarif</a></li>
                <li class="nav-item"><a class="navbar-brand" href="" data-toggle="modal" data-target="#votreAvis">Votre avis</a></li>
                <li class="nav-item"><a class="navbar-brand" href="" data-toggle="modal" data-target="#votreAvis">Tada</a></li>
                <?php
                //  Vérifie si on est connecté
                    if (isset($_SESSION["idClient"])){
                ?>
                    <li class="nav-item"><a class="navbar-brand" href="index.php?v=profil" ?> Mon profil</a></li>
                <?php
                
                    } else {
                ?>
                    <li class="nav-item"><a class="navbar-brand" href="index.php?v=auth">Bonjour</a></li>
                <?php
                    }
                ?>
                <!--Formulaire pour le moteur de recherche -->
                <!--<li class="nav-item">
                    <form action="index.php"  class="form-inline" method="post">
                        <input class="form-control mr-sm-2" name="recherche" type="search" placeholder="Recherche..." aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                    </form>-->
                </li>
            </ul>
        </div>
    </nav>
