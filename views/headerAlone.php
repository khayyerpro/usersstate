<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Test de titre de page du cours PhpN</title>
    <meta name="description" content="Test de description">
    <meta name="keywords" content="planning, formation, tache, todolist, gestion de temps, gestion de contact, addiction">
    <!-- Indexer et suivre -->
    <meta name="robots" content="index,follow">
    <!-- Pour dimensionner la page par defaut -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Police du site -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Questrial&display=swap" rel="stylesheet"> 
    <!-- Bootstrap 5.2 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous"> -->
    <script src="../component/js/bootstrap.js"  crossorigin="anonymous"></script>

    <!-- Surcharge des Css -->
    <link href="../component/css/cssSurchargeGlobale.css" rel="stylesheet">

</head>