<footer>
    <!-- Barre de navigation pied de page  -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mt-4 mb-4">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Menu deroulant contenant toute les pages de renseignement -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="col">
                <ul>
                    <li><a href="index.php?v=rgpd" target="_blank">RGPD</a></li>
                    <li><a href="index.php?v=contact" target="_blank">Contact</a></li>
                    <li><a href="index.php?v=faq" target="_blank">FAQ</a></li>
                </ul>
            </div>

            <div class="col">
                <ul>
                    <li><a href="https://twitter.com/Igestpro"target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com/igestpro/"target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="index.php?v=mentions" target="_blank"><i class="far fa-envelope-open"></i></a></li>
                    <li><a href="index.php?v=mentions" target="_blank"><i class="fas fa-phone-alt"></i></a></li>
                    <li><a href="index.php?v=mentions" target="_blank"><i class="fas fa-at"></i></a></li>
                </ul>
            </div>

            <div class="col">
                <ul>
                    <li><a href="index.php?v=mentions" target="_blank">Mention Legale</a></li>
                    <li><a href="index.php?v=copyright" target="_blank">Copyright</a></li>
                    <li><a href="index.php?v=cookies" target="_blank">Cookies</a></li>
                    <li><a href="index.php?v=cgv" target="_blank">Cgv</a></li>
                    <li><a href="index.php?v=confidentialite" target="_blank">Confidentialité</a></li>
                </ul>
            </div>
        </div>
    </nav>
</footer>
</body>
</div>
</html>