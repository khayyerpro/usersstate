<?php
// Notre code
?>

<div class="row justify-content-center mt-4 mb-3">

    <div class="card cardListeServices bg-light mb-5 ml-3" id="idtest">
        <h3 class="card-header">Nom un</h3>
        <div class="card-body text-center">
            <div class="card-text card-text-services">
                <ul>Texte d'exemple</ul>
            </div>
            <button type="button" class="btn btn-success btn-lg" >
            <i class="fas fa-arrow-alt-circle-right" ></i>&nbsp;Texte bouton</button>
        </div>
    </div>

    <div class="card cardListeServices bg-light mb-5 ml-3" id="idtest">
        <h3 class="card-header">Nom deux</h3>
        <div class="card-body text-center">
            <div class="card-text card-text-services">
                <ul>Texte d'exemple</ul>
            </div>
            <button type="button" class="btn btn-success btn-lg" >
            <i class="fas fa-arrow-alt-circle-right" ></i>&nbsp;Texte bouton</button>
        </div>
    </div>

    <div class="card cardListeServices bg-light mb-5 ml-3" id="idtest">
        <h3 class="card-header">Nom trois</h3>
        <div class="card-body text-center">
            <div class="card-text card-text-services">
                <ul>Texte d'exemple</ul>
            </div>
            <button type="button" class="btn btn-success btn-lg" >
            <i class="fas fa-arrow-alt-circle-right" ></i>&nbsp;Texte bouton</button>
        </div>
    </div>
</div>